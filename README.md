casperToC360 is an intent to migrate casper data to c360.

Application consists of 2 parts:
	1. App scheduler (Shell)
	2. App query runner (Java)

Application symentics:

	App scheduler:
		Scheduler schedules the next run for the application. The scheduler queries an application table in the application_tracker_db database to store application metadata. Scheduler also executes the "LOAD DATA INPATH.." query to load the data to the table.

	App query runner:
		Query runner is a yaml based java app which will run queries according to the yaml configuration. Yaml consists of 2 parts "dataExtractor" and stages.
		When data needs to be migrated the scheduler calls the jar with the "dataExtractor" pararms. i.e : "java -jar jarPath DE toDate fromDate yamlPath" and writes data to a file.

	Note: Data you need to migrate should have some date column on which a range query can be done to get data between a certain time, in our case toDate and fromDate.

		Scheduler then loads that file in to the table and now calls same jar with the query params i.e : "java -jar jarPath QUERY yamlPath". 

		Query runner's yaml also has a section called "stages" which will have certain stages within for logical separation of queries. Within each stage we can 1 or more queries, transcations are committed after each query executed. When jar is called with the QUERY param, the jar starts executing the stages one by one.  
		
	Note: All queries in the stages should are executeUpdate() i.e: INSERT, UPDATE, DELETE, DROP, TRUNCATE, CREATE. SELECT is not supported.

Sample Yaml attached in "src/main/resources/" folder
