package com.data.migration.dto;

public class QueryDetails {
    private int queryId;
    private String query;
    private String validationQuery;
    private String fallbackQuery;
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public int getQueryId() {
        return queryId;
    }

    public void setQueryId(int queryId) {
        this.queryId = queryId;
    }

    public String getFallbackQuery() {
        return fallbackQuery;
    }

    public void setFallbackQuery(String fallbackQuery) {
        this.fallbackQuery = fallbackQuery;
    }

    public String getValidationQuery() {
        return validationQuery;
    }

    public void setValidationQuery(String validationQuery) {
        this.validationQuery = validationQuery;
    }


    @Override
    public String toString() {
        return String.format("queryId:%s, query:%s, validationQuery:%s, fallbackQuery:%s, type:%s", queryId, query, validationQuery, fallbackQuery,type);
    }
}
