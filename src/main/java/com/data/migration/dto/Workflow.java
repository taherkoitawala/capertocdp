package com.data.migration.dto;

import java.util.List;

public class Workflow {
    private List<ConnectionDetails> connections;
    private List<StageDetails> stages;
    private NotificationDetails notification;
    private DataDetails dataExtractor;

    public List<ConnectionDetails> getConnections() {
        return connections;
    }

    public void setConnections(List<ConnectionDetails> connections) {
        this.connections = connections;
    }

    public List<StageDetails> getStages() {
        return stages;
    }

    public void setStages(List<StageDetails> stages) {
        this.stages = stages;
    }

    public NotificationDetails getNotification() {
        return notification;
    }

    public void setNotification(NotificationDetails notification) {
        this.notification = notification;
    }

    public DataDetails getDataExtractor() {
        return dataExtractor;
    }

    public void setDataExtractor(DataDetails dataExtractor) {
        this.dataExtractor = dataExtractor;
    }

}
