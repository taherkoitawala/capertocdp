package com.data.migration.dto;

public class NotificationDetails {
    String url;
    String user;
    String password;
    String database;
    Integer appId;
    String applicationsTable;
    String applicationTrackerTable;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getApplicationsTable() {
        return applicationsTable;
    }

    public void setApplicationsTable(String applicationsTable) {
        this.applicationsTable = applicationsTable;
    }

    public String getApplicationTrackerTable() {
        return applicationTrackerTable;
    }

    public void setApplicationTrackerTable(String applicationTrackerTable) {
        this.applicationTrackerTable = applicationTrackerTable;
    }

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    @Override
    public String toString() {
        return String.format("url: %s user: %s password: ****** database: %s appId:%s applicationsTable: %s applicationTrackerTable: %s", url, user, database,appId, applicationsTable, applicationTrackerTable);
    }
}
