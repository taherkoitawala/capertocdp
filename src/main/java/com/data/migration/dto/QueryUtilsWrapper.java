package com.data.migration.dto;

import com.data.migration.workflow.execution.WorkflowExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class QueryUtilsWrapper {
    Logger LOG = LoggerFactory.getLogger(WorkflowExecutor.class);
    private Connection connection;
    private Statement statement;
    private CallableStatement callableStatement;
    private String type;

    private Statement createStatement(Connection connection) throws SQLException {
        LOG.info("Creating new statement for type:{}", type);
        statement = connection.createStatement();
        return statement;
    }

    private Statement createStatement(Connection connection, int fetchSize) throws SQLException {
        LOG.info("Creating new statement for type:{} with fetch size", type, fetchSize);
        statement = connection.createStatement();
        statement.setFetchSize(fetchSize);
        return statement;
    }

    public Statement getStatement(Connection connection) throws SQLException {
        return checkNotNull(statement) ? statement : createStatement(connection);
    }

    public Statement getStatementWithFetchSize(Connection connection, int fetchSize) throws SQLException {
        if (checkNotNull(statement)&&fetchSize!=0) {
            statement.setFetchSize(fetchSize);
            return statement;
        } else {
            return createStatement(connection, fetchSize);
        }
    }

    public CallableStatement getCallableStatement(Connection connection, String query) throws SQLException {
        LOG.info("Creating new callable statement");
        return checkNotNull(callableStatement) ? callableStatement : createCallableStatement(connection, query);
    }

    private CallableStatement createCallableStatement(Connection connection, String query) throws SQLException {
        LOG.info("Creating new callablestatement for type:{}", type);
        return callableStatement = connection.prepareCall(query);
    }

    public Connection getConnection() {
        return this.connection;
    }

    public void setConnection(Connection connection, String type) throws SQLException {
        if (connection != null) {
            LOG.info("Set Connection for type:{}", type);
            this.connection = connection;
            this.type = type;
        }

    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    public boolean checkNotNull(Statement statement) {
        return statement != null;
    }

    public void closeAllHandles() {
        LOG.info("Cleaning all resources");
        try {
            close(statement);
            close(callableStatement);
            close(connection);
        } catch (SQLException e) {
            LOG.error("Error closing handles cause:{} message:{}", e.getCause(), e.getMessage());

        }

    }

    private void close(Statement statement) throws SQLException {
        if (checkNotNull(statement)) {
            statement.close();
        }
    }

    private void close(Connection connection) throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

    private void setAutoCommit(boolean flag) throws SQLException {
        this.connection.setAutoCommit(flag);
    }
}
