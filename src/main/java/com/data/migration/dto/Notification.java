package com.data.migration.dto;

public enum Notification {
    STARTED,
    RUNNING,
    COMPLETED,
    FAILED
}
