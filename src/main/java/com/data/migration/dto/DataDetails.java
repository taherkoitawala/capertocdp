package com.data.migration.dto;

public class DataDetails {
    String query;
    String type;
    String destination;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    @Override
    public String toString() {
        return String.format("Query: %s Type: %s destination: %s", query, type, destination);
    }
}
