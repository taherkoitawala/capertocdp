package com.data.migration.dto;

import java.util.List;

public class StageDetails {
    private Integer stageId;
    private List<QueryDetails> queries;

    public Integer getStageId() {
        return stageId;
    }

    public void setStageId(Integer stageId) {
        this.stageId = stageId;
    }

    public List<QueryDetails> getQueries() {
        return queries;
    }

    public void setQueries(List<QueryDetails> queries) {
        this.queries = queries;
    }

    @Override
    public String toString() {
        return "stageId:" + stageId + " queries: " + queries.toString();
    }
}
