package com.data.migration.contants;

public class Constants {

    public static final String notificaiton="Notification";
    public static final String updateNotificaiton="update %s set stage_id=%s,stage_status='%s' where app_id=%s and job_id=%s";
    public static final String createJobId="select max(job_id) from %s";
    public static final String select="select";
    public static final String update="update";
    public static final String insert="insert";
    public static final String delete="delete";
    public static final String load="load";
    public static final String extactDataTofile="SELECT * FROM %s INTO OUTFILE '%s' FIELDS TERMINATED BY ','";
}
