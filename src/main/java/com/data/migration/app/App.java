package com.data.migration.app;

import com.data.migration.dto.Workflow;
import com.data.migration.workflow.execution.WorkflowExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.TypeDescription;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class App {
    static Logger LOG = LoggerFactory.getLogger(App.class);
    private Workflow workflow;

    public static void main(String[] args) throws Exception {
        App app = new App();
        Workflow workflow = null;
        WorkflowExecutor executor = new WorkflowExecutor();

        if (args[0].equals("DE")) {
            if (args.length == 4) {
                workflow = app.read(new File(args[3]));
                System.out.println(workflow.getDataExtractor().getQuery());
                executor.extractData(workflow,args[1],args[2]);
            } else {
                throw new IllegalArgumentException("DE  needs 2 more args toDate ,fromDate and yaml file path");
            }

        } else if (args[0].equals("QUERY")) {
            if (args.length == 2) {
                workflow = app.read(new File(args[1]));
                executor.startExecution(workflow);
            } else {
                throw new IllegalArgumentException("QUERY  needs 1 more arg yaml file path");
            }
        } else {
            throw new IllegalArgumentException("App needs arg1 as DE/ QUERY");
        }
    }

    public Workflow read(File yamlFile) throws FileNotFoundException {
        if (!yamlFile.isDirectory()) {
            Constructor constructor = new Constructor(Workflow.class);
            TypeDescription customTypeDescription = new TypeDescription(Workflow.class);
            customTypeDescription.addPropertyParameters("", Workflow.class);
            constructor.addTypeDescription(customTypeDescription);
            Yaml yaml = new Yaml(constructor);
            InputStream inputStream = new FileInputStream(yamlFile);
            workflow = yaml.load(inputStream);
            return workflow;
        } else {
            LOG.error("File path:{} should not be a directory", yamlFile.getAbsoluteFile());
            System.exit(-1);
            return null;
        }

    }
}
