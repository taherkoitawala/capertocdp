package com.data.migration.workflow.execution;

import com.data.migration.connection.ConnectionHelper;
import com.data.migration.contants.Constants;
import com.data.migration.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class WorkflowExecutor {


    private static Logger LOG = LoggerFactory.getLogger(WorkflowExecutor.class);
    private final String testConnectionQuery = "SELECT 'TEST_CONNECTION'";
    private Map<String, QueryUtilsWrapper> connectionMap = new HashMap<>();
    private int jobId;
    private BufferedWriter writer = null;
    private FileWriter fileWriter = null;

    public void extractData(final Workflow workflow, String fromDate, String toDate) {
        QueryUtilsWrapper utils = null;
        try {
            if (testAllConnections(workflow)) {
                jobId = getJobId(workflow.getNotification());
                notifyStatus(workflow.getNotification(), jobId, 1, Notification.STARTED.name());
                notifyStatus(workflow.getNotification(), jobId, 1, Notification.RUNNING.name());
                utils = getFromCachce(workflow.getDataExtractor().getType());
                Connection connection = utils.getConnection();
                String query = String.format(workflow.getDataExtractor().getQuery(), fromDate, toDate);
                LOG.info("Prepared data extraction query: {}", query);
                ResultSet resultSet = QueryExecutor.executeQuery(utils.getStatement(connection), query);
                writeToFile(resultSet, String.format(workflow.getDataExtractor().getDestination(), toDate.split(" ")[0], jobId));
                notifyStatus(workflow.getNotification(), jobId, 1, Notification.COMPLETED.name());
                //connection.commit();
            } else {
                failed("Terminating due to failures.....");
            }
        } catch (SQLException e) {
            notifyStatus(workflow.getNotification(), jobId, 1, Notification.FAILED.name());
            utils.closeAllHandles();
            LOG.error("Execution failed  at query {} cause{} state {} message {}", workflow.getDataExtractor().getQuery(), e.getCause(), e.getSQLState(), e.getMessage());
        } catch (IllegalStateException e) {
            LOG.error("Execution failed  at message {} cause {}", e.getMessage(), e.getCause());
            notifyStatus(workflow.getNotification(), jobId, 1, Notification.FAILED.name());
            utils.closeAllHandles();
        } catch (Exception e) {
            LOG.error("Execution failed  at message {} cause {}", e.getMessage(), e.getCause());
            notifyStatus(workflow.getNotification(), jobId, 1, Notification.FAILED.name());
            utils.closeAllHandles();
        }

    }

    public void startExecution(final Workflow workflow) {
        LOG.info("Started workflow execution");
        LOG.info("Testing connections to all databases");
        if (testAllConnections(workflow)) {
            jobId = getJobId(workflow.getNotification());
            executeStages(workflow);
            completed();
        } else {
            failed("Terminating due to failures.....");
        }
    }

    private void executeStages(final Workflow workflow) {
        List<StageDetails> stages = workflow.getStages();
        Iterator<StageDetails> iterator = stages.iterator();
        String query = null;
        QueryUtilsWrapper queryUtils = null;
        Connection connection = null;
        List<QueryDetails> queries;
        QueryDetails queryDetails;
        int stageCounter = 0;
        try {
            while (iterator.hasNext()) {
                StageDetails stageDetails = iterator.next();
                stageCounter = stageDetails.getStageId();

                notifyStatus(workflow.getNotification(), jobId, stageCounter, Notification.STARTED.name());
                notifyStatus(workflow.getNotification(), jobId, stageCounter, Notification.RUNNING.name());
                queries = stageDetails.getQueries();
                for (QueryDetails details : queries) {
                    queryDetails = details;
                    queryUtils = getFromCachce(queryDetails.getType());
                    connection = queryUtils.getConnection();
                    Statement statement = queryUtils.getStatement(connection);
                    query = queryDetails.getQuery();
                    execute(statement, query);
                    LOG.info("Query {} completed committing query results", details.getQuery());
                    connection.commit();
                }
                notifyStatus(workflow.getNotification(), jobId, stageCounter, Notification.COMPLETED.name());
                LOG.info("Stage {} completed ", stageCounter);
            }
        } catch (SQLException e) {
            notifyStatus(workflow.getNotification(), jobId, stageCounter, Notification.FAILED.name());
            LOG.error("Query {} failed while executing... Rolling back ", query);
            rollback(connection);
            queryUtils.closeAllHandles();
            LOG.error("Execution failed  at query {} cause{} state {} message {}", query, e.getCause(), e.getSQLState(), e.getMessage());
        } catch (IllegalStateException e) {
            LOG.error("Execution failed  at message {} cause {}", e.getMessage(), e.getCause());
            notifyStatus(workflow.getNotification(), jobId, stageCounter, Notification.FAILED.name());
            LOG.error("Query {} failed while executing... Rolling back ", query);
            rollback(connection);
            queryUtils.closeAllHandles();
        } catch (Exception e) {
            LOG.error("Execution failed  at message {} cause {}", e.getMessage(), e.getCause());
            notifyStatus(workflow.getNotification(), jobId, stageCounter, Notification.FAILED.name());
            LOG.error("Query {} failed while executing... Rolling back ", query);
            rollback(connection);
            queryUtils.closeAllHandles();
        }
    }

    private void rollback(Connection connection) {
        try {
            if (connection != null)
                connection.rollback();
        } catch (SQLException e) {
            LOG.info("Error while rolling back message {} cause {}", e.getMessage(), e.getCause());
        }
    }

    private void execute(Statement statement, String query) throws SQLException, IOException {
        QueryExecutor.executeUpdate(statement, query);
    }

    private void writeToFile(ResultSet resultSet, String source) throws IOException, SQLException {
        LOG.info("File path: {}", source);
        Path path = Paths.get(source);
        File file = path.toFile();
        if (!file.exists()) {
            LOG.info("File {} does not exists..... Creating file now. ", file.getAbsoluteFile());
            Files.createDirectories(path.getParent());
            file.createNewFile();
        }
        writer = new BufferedWriter(new FileWriter(file));
        int colCount = resultSet.getMetaData().getColumnCount();
        while (resultSet.next()) {
            String line = "";
            for (int i = 1; i <= colCount; i++) {
                if (resultSet.getString(i) != null) {
                    line = line + String.format("\"%s\",", resultSet.getString(i));

                } else {
                    line = line + ",";
                }
            }
            writer.write(line);
            writer.newLine();
        }
        writer.flush();
        writer.close();

    }

    private Integer getJobId(NotificationDetails notificationDetails) {
        QueryUtilsWrapper queryUtils = connectionMap.get(Constants.notificaiton);
        try {
            int jobId;
            String query = String.format(Constants.createJobId, notificationDetails.getApplicationTrackerTable());
            LOG.info("Fetching jobid with query {}", query);
            ResultSet resultSet = queryUtils.getStatement(queryUtils.getConnection()).executeQuery(query);
            resultSet.next();
            jobId = resultSet.getInt(1);
            LOG.info("Fetched jobid {}", jobId);
            return jobId;
        } catch (SQLException e) {
            LOG.error("Error creating job id message {} sqlState{} cause {}", e.getMessage(), e.getSQLState(), e.getCause());
            queryUtils.closeAllHandles();
            System.exit(-1);
            return 0;
        }


    }

    private void notifyStatus(NotificationDetails notificationDetails, final int jobId, final int stageCounter, final String notifyStatus) {
        String updateQuery = null;
        try {
            updateQuery = String.format(Constants.updateNotificaiton, notificationDetails.getApplicationTrackerTable(), stageCounter, notifyStatus, notificationDetails.getAppId(), jobId);
            QueryUtilsWrapper queryUtils = connectionMap.get(Constants.notificaiton);
            Connection connection = queryUtils.getConnection();
            Statement statement = queryUtils.getStatement(connection);
            QueryExecutor.executeUpdate(statement, updateQuery);
            LOG.info("Status '{}' notified for stage: {} jobid: {} ", notifyStatus, stageCounter, jobId);
        } catch (SQLException e) {
            LOG.error("Notification failed query:{},stage:{},job:{},cause:{},message:{},state:{}", updateQuery, stageCounter, jobId, e.getCause(), e.getMessage(), e.getSQLState());
        }

    }

    private boolean testAllConnections(Workflow workflow) {
        if (testConnections(workflow) && testConnectionToNotificationDB(workflow.getNotification())) {
            LOG.info("All test connections to all databases passed.");
            return true;
        } else
            return false;
    }

    private boolean testConnections(Workflow workflow) {
        Connection connection = null;
        Statement statement = null;
        boolean flag = false;
        QueryUtilsWrapper queryUtilsWrapper;
        List<ConnectionDetails> connections = workflow.getConnections();
        for (ConnectionDetails connectionDetails : connections) {
            queryUtilsWrapper = new QueryUtilsWrapper();
            try {
                connection = new ConnectionHelper.Builder().jdbcUrl(connectionDetails.getUrl()).user(connectionDetails.getUser()).password(connectionDetails.getPassword()).database(connectionDetails.getDatabase()).build().getConnection();
                connection.setAutoCommit(false);
                queryUtilsWrapper.setConnection(connection, connectionDetails.getType());
                statement = connection.createStatement();
                queryUtilsWrapper.setStatement(statement);
                statement.execute(testConnectionQuery);
                flag = true;
                LOG.info("Test connection for type:{} passed", connectionDetails.getType());
                addToMap(connectionDetails.getType(), queryUtilsWrapper);
            } catch (Exception e) {
                LOG.error("Test connection failed for the following params connection type:{} url:{},user:{},password:{} database:{}, cause: {}", connectionDetails.getType(), connectionDetails.getUrl(), connectionDetails.getUser(), "***********", connectionDetails.getDatabase(), e.getMessage());
                LOG.info("Stopping application....");
                close(statement);
                close(connection);
            }
        }
        LOG.debug("Added connections are {}", connectionMap.toString());
        return flag;

    }

    private void addToMap(String connectionName, QueryUtilsWrapper queryUtilsWrapper) {
        connectionMap.put(connectionName, queryUtilsWrapper);
    }

    private QueryUtilsWrapper getFromCachce(String connectionName) {
        if (connectionMap.containsKey(connectionName)) {
            LOG.info("Got connection for type:{}", connectionName);
            return connectionMap.get(connectionName);
        } else {
            LOG.error("No Connection found for type:{} ", connectionName);
            throw new NoSuchElementException("Connection : " + connectionName + " not found in cache");
            //return null;
        }

    }

    private boolean testConnectionToNotificationDB(NotificationDetails notification) {
        boolean flag = false;
        QueryUtilsWrapper queryUtilsWrapper;
        Connection connection = null;
        Statement statement = null;
        try {
            queryUtilsWrapper = new QueryUtilsWrapper();
            connection = new ConnectionHelper.Builder().jdbcUrl(notification.getUrl()).user(notification.getUser()).password(notification.getPassword()).database(notification.getDatabase()).build().getConnection();
            queryUtilsWrapper.setConnection(connection, Constants.notificaiton);
            statement = connection.createStatement();
            queryUtilsWrapper.setStatement(statement);
            statement.execute(testConnectionQuery);
            addToMap(Constants.notificaiton, queryUtilsWrapper);
            LOG.info("Test connection for type:{} passed", Constants.notificaiton);
            flag = true;
        } catch (Exception e) {
            LOG.error("Test connection to notificaiton tables failed for the following params url:{},user:{},password:{} database:{}, cause: {}", notification.getUrl(), notification.getUser(), "***********", notification.getDatabase(), e.getMessage());
            LOG.info("Stopping application....");
            close(statement);
            close(connection);
        }

        return flag;
    }

    private void close(Connection connection) {
        try {
            if (connection != null) {
                LOG.info("Closing connections");
                connection.close();
            }

        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }


    }

    private void close(Statement statement) {
        try {
            if (statement != null) {
                LOG.info("Closing statement");
                statement.close();
            }

        } catch (SQLException e) {
            LOG.error(e.getMessage());
        }
    }

    private void close(Map<String, QueryUtilsWrapper> connectionMap) {
        LOG.info("Closing all connections now");
        for (String key : connectionMap.keySet()) {
            LOG.info("Closing all connections for type:{}", key);
            connectionMap.get(key).closeAllHandles();

        }

    }

    private void completed() {
        LOG.info("All stages completed and notified shutting down...");
        close(connectionMap);
    }

    private void failed(String errorMsg) {
        LOG.error(errorMsg);
        close(connectionMap);
        LOG.info("Shutdown Complete");
        System.exit(-1);

    }
}
