package com.data.migration.workflow.execution;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class QueryExecutor {

   static Logger LOG = LoggerFactory.getLogger(QueryExecutor.class);

    public static void executeUpdate(Statement statement, String query) throws SQLException{
        LOG.info("Executing query {}", query);
            statement.executeUpdate(query);

    }

    public static ResultSet executeQuery(Statement statement, String query) throws SQLException {
        LOG.info("Executing query : {}", query);
        return statement.executeQuery(query);
    }
}
