package com.data.migration.connection;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionHelper {

    private final String url;
    private final String password;
    private final String user;
    private final String database;
    private final String connectionClass;

    public static class Builder
    {
        private String url;
        private String password;
        private String user;
        private String database="default";
        private String connectionClass="com.mysql.jdbc.Driver";

        public Builder jdbcUrl(String url)
        {
            this.url=url;
            return  this;
        }
        public Builder password(String password)
        {
            this.password=password;
            return this;
        }
        public Builder user(String user)
        {
            this.user=user;
            return this;
        }
        public Builder database(String database)
        {
            this.database=database;
            return  this;
        }
        public ConnectionHelper build() throws Exception
        {
            return new ConnectionHelper(this);
        }
        public Builder withConnectionClass(String connectionClass)
        {
            this.connectionClass=connectionClass;
            return this;
        }

    }

    private ConnectionHelper(Builder builder) throws Exception
    {
        this.url=builder.url;
        this.user=builder.user;
        this.password=builder.password;
        this.database=builder.database;
        this.connectionClass=builder.connectionClass;
    }

     public Connection getConnection() throws Exception
    {
        Class.forName(this.connectionClass).newInstance();
        return DriverManager.getConnection(url+database,user,password);

    }
}
