set -f
#set -x
appTable="application_tracker_db.applications"
appMetadata="application_tracker_db.application_tracker"
database=""
#results="FAILED 1 3"
maxJobIdQuery="select max(job_id) from $appMetadata where app_id="
checkJobQuery="select stage_status,stage_id,has_stages,to_date,from_date from $appTable app join $appMetadata tracker on (app.app_id=tracker.app_id) where job_id =$maxJobId"
maxStagesQuery="select has_stages from $appTable where app_id="
hasStages=""
running="RUNNING"
failed="FAILED"
completed="COMPLETED"
toDate=""
fromDate=""
filePath="/home/application/casperToCdp/"
fileName="/dest.csv"
insertNextJobId="insert into $appMetadata(app_id,job_id,to_date,from_date,stage_id,stage_status) values(\$appId,\$maxJobId,'\$toD','\$frmD',\$stageId,'\$status')"
stagingTable="demographic_store.intermediate_casper_to_cdp"
loadFromFile="LOAD DATA LOCAL INFILE '\$filePath' INTO TABLE $stagingTable FIELDS TERMINATED BY ',' ENCLOSED BY '\"' ESCAPED BY '\"' LINES TERMINATED BY '\n'"
nextDateQuery="SELECT DATE_SUB(NOW(), INTERVAL 5 MINUTE)"
recordCountQuery="update $appMetadata set record_count=(select count(*) from demographic_store.intermediate_casper_to_cdp),runtime=NOW() where job_id="
jarName=""
runJar="java -jar"
args=""
appId=""
jarPath=""
yamlPath=""
maxJobId=""
preconditionsCheck()
{
        if [ $1 != 1 -o -z "$1" ] ; then
                echo "Error:$2" ;
                exit 0;
        fi
}

run()
{
        preconditionsCheck $(($# == 5)) "Script needs 5 variables mysql login profile, database, appId, jar path, yaml path "
        login_profile=$1
        echo "C360 profile: $login_profile"
        database=$2
        echo "C360 database: $database"
        appId=$3
        echo "AppId: $appId"
        jarPath=$4
        echo "Jar Path: $jarPath"
        yamlPath=$5
        echo "Yaml Path: $yamlPath"
        hasStages=$(executeWithReturnC360 "$maxStagesQuery$appId")
        echo "App has $hasStages stages"
        maxJobId=$(executeWithReturnC360 "$maxJobIdQuery$appId")
        echo "Max job id: $maxJobId"
        local res=$(executeWithReturnC360 "$checkJobQuery$maxJobId")
        echo "For id : $maxJobId, job result : $res"
	local arr=(`echo ${res}`);
        jobStatus "${arr[@]}"

}

executeWithReturnC360()
{
	echo "Executing : $1" >&2
       local results=$(echo $1 | mysql --login-path=$login_profile -D$database -N)
	echo $results
}

executeWithReturnCasper()
{
    local results=$(echo $1 | mysql --login-path=$casperProfile -D$casperDatabase -N)
        echo $results
}
jobStatus()
{
        local arr=("$@")
	preconditionsCheck $((${#arr[@]} == 7)) "Either stage_status or stage_id or has_stages or from_date or to_date is missing here"
        for s in "${arr[@]}"; do
        #lastJobId=${arr[1]}
        lastJobId=3
                if [ "$s" == "$failed" ]; then
                        echo "Previously running job: maxJobId has failed at stage ${arr[1]}";
                elif [ "$s" == "$running" ]; then
                        echo "Job job_id : $maxJobId already running will retry execution later";
                        exit;
                elif [ "$s" == "$completed" ]; then
                        if [ $hasStages -eq $lastJobId ]; then
                                echo "Last job was fully completed. $hasStages of $lastJobId were completed.";
                                echo "Scheduling next Data Extaction";
                                echo ""
                                runNextJob "${arr[3]} ${arr[4]}" "${arr[5]} ${arr[6]}"
                        else
                            if [ 1 -eq $lastJobId ]; then
                                echo "Previous job only ran data Extaction... ";

                            else
                                echo "$hasStages of $lastJobId are completed. Something wrong with execution exiting"
                                exit
                            fi
                        fi

                fi
        done
}

runNextJob()
{

        toD="$1"
        frmD="$2"
	local nextJobId=$(( $maxJobId + 1 ))
	local nextDate=$(executeWithReturnC360 "$nextDateQuery")
        fDate="$(echo $nextDate | cut -d ' ' -f 1)/$nextJobId"
        echo "folderdate $fDate"
        local path=$filePath$fDate
        fullPath=$filePath$fDate$fileName
        echo $fullPath
        echo "$maxJobId $nextJobId"
        local insertNextJobId=${insertNextJobId//\$appId/$appId}
        local insertNextJobId=${insertNextJobId//\$maxJobId/$nextJobId}
        local insertNextJobId=${insertNextJobId//\$stageId/1}
	local failed=$insertNextJobId
	local running=$insertNextJobId
        local insertNextJobId=${insertNextJobId//\$status/"COMPLETED"}
        local insertNextJobId=$(echo $insertNextJobId | sed "s/\$toD/$nextDate/g")
        local insertNextJobId=$(echo $insertNextJobId | sed "s/\$frmD/$toD/g")
	echo "Check $insertNextJobId"
	local failed=${failed//\$status/"FAILED"}
        local running=${running//\$status/"RUNNING"}
        local loadFromFile=${loadFromFile//\$filePath/$fullPath}
	 $(executeWithReturnC360 "$insertNextJobId")
         scheduleDE "$toD" "$nextDate"
	test -f $fullPath
	local error=$?
        if [ $error != 1 -o -z "$1" ] ; then
                $(executeWithReturnC360 "$loadFromFile")
		query="update $appMetadata set to_date='$nextDate',from_date='$toD',stage_id=2 where app_id =$appId and job_id=$nextJobId"
		$(executeWithReturnC360 "$query")
		$(executeWithReturnC360 "$recordCountQuery$nextJobId")
		scheduleQuery
		rm -rf $fullPath
		echo "*************************COMPLETED*************************************"
	else
		$(executeWithReturnC360 "$failed")	
        fi


}
scheduleDE()
{
        toDate="$1"
        fromDate="$2"
       java -jar $jarPath DE "$toDate" "$fromDate" $yamlPath
	query="update $appMetadata set to_date='$toDate',from_date='$fromDate' where app_id =$appId and job_id='$maxJobId'"
#	$(executeWithReturnC360 "$query")

}
scheduleQuery()
{
    java -jar $jarPath QUERY $yamlPath
}

run $@

